/*
 * CFile.h
 *
 *  Created on: 3 nov. 2016
 *      Author: user
 */

#include <list>
#include <iostream>
#include "CList.h"
using namespace std;
#ifndef CFILE_H_
#define CFILE_H_

class CFile: public CList {
public:
	CFile();
	virtual ~CFile();
	CList& ajouter(int);
};

#endif /* CFILE_H_ */
