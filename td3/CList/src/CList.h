/*
 * CList.h
 *
 *  Created on: 3 nov. 2016
 *      Author: user
 */
#include <list>
#include <iostream>
using namespace std;
#ifndef CLIST_H_
#define CLIST_H_

class CList {
public:
	CList();
	virtual ~CList();

	void operator>(int i);
	void afficher(std::ostream &flux) const;
	friend ostream& operator<<(ostream &flux, list<int> liste);
	virtual CList& ajouter(int) =0;
	friend CList& operator<(CList& i);
private :
	list<int> liste;
};

#endif /* CLIST_H_ */
