/*
 * main.cpp
 *
 *  Created on: 20 oct. 2016
 *      Author: user
 */


#include "segment.h"

int main() {
	Point* p = new Point(42, 78.0312);
	Point * c = new Point(1,2.3);
	p->afficher();
	p->cloner(c);
	p->afficher();

	delete (c);
	delete (p);

	coloredPoint * c1 = new coloredPoint();
	coloredPoint * c2 = new coloredPoint(c,"yellow");
	c1->afficher();
	c2->afficher();
	segment* a = new segment(*c1,*c2);
	a->afficher();
	return 0;
}

