/*
 * shape.h
 *
 *  Created on: 20 oct. 2016
 *      Author: user
 */

#ifndef SHAPE_H_
#define SHAPE_H_

#include "coloredPoint.h"
class shape {
	public:
		shape(void);
		virtual ~shape();
		//fonctions virtuelles
		virtual void deplacer(float x, float y) = 0;
		virtual void afficher() = 0;

};

#endif /* SHAPE_H_ */
