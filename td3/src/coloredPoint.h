/*
 * coloredPoint.h
 *
 *  Created on: 20 oct. 2016
 *      Author: user
 */

#ifndef COLOREDPOINT_H_
#define COLOREDPOINT_H_
#include "Point.h"
class coloredPoint : public Point {
private :
	string color;
public:
	coloredPoint(void);
	coloredPoint(Point a, string c);
	coloredPoint(float x, float y, string c);
	coloredPoint(const Point &c);
	void afficher();
	virtual ~coloredPoint();
};

#endif /* COLOREDPOINT_H_ */
