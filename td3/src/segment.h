/*
 * segment.h
 *
 *  Created on: 20 oct. 2016
 *      Author: user
 */

#ifndef SEGMENT_H_
#define SEGMENT_H_
#include "shape.h"

class segment : public shape {
	coloredPoint a;
	coloredPoint b;

public:
	segment();
	segment(const coloredPoint &c1,const coloredPoint &c2);
	void deplacer(float x, float y);
	void afficher();

	virtual ~segment();
};

#endif /* SEGMENT_H_ */
