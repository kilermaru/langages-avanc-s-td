/*
 * Point.h
 *
 *  Created on: 20 oct. 2016
 *      Author: user
 */

#ifndef POINT_H_
#define POINT_H_
#include <iostream>
using namespace std;


class Point {
private :
	float x;
	float y;
public :
		Point ();
		Point (float x, float y);
		Point (Point* a);
		void afficher();
		void cloner(const Point &a);
		float getX() const;
		float getY() const;
		virtual ~Point();
};

#endif /* POINT_H_ */

