/*
 * Point.h
 *
 *  Created on: 22 sept. 2016
 *      Author: user
 */
#ifndef POINT_H_
#define POINT_H_

#include <iostream>
using namespace std;

class Point {
private:
	float x;
	float y;

public:
	Point(void);
	virtual ~Point();

	Point(float x,float y);
	Point (const Point &p);

	virtual float getX()const;
	virtual float getY()const;

	virtual void deplacePoint(const float dx, const float dy);

	void Afficher()const;
	Point cloner();
};

#endif /* POINT_H_ */
