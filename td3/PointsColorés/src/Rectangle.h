/*
 * Rectangle.h
 *
 *  Created on: 2 nov. 2016
 *      Author: Yoann
 */

#ifndef RECTANGLE_H_
#define RECTANGLE_H_

#include "Forme.h"
#include "PointColor.h"

class Rectangle : public Forme {
private:
	PointColor pc1;
	PointColor pc2;
	PointColor pc3;
	PointColor pc4;

public:
	Rectangle(const PointColor& p1,const PointColor& p2, const PointColor& p3, const PointColor& p4);
	virtual ~Rectangle();

	void deplacer(float dx, float dy);
	void afficher();
};

#endif /* RECTANGLE_H_ */
