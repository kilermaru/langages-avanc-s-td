/*
 * Segment.cpp
 *
 *  Created on: 19 oct. 2016
 *      Author: user
 */

#include "Segment.h"

Segment::Segment(const PointColor &p1,const PointColor &p2): pc1(p1),pc2(p2){}

Segment::Segment():pc1(),pc2(){}

Segment::~Segment() {
	// TODO Auto-generated destructor stub
}

//methode
void Segment::deplacer(const float dx,const float dy){
	pc1.deplacePoint(dx,dy);
	pc2.deplacePoint(dx,dy);
}

void Segment::afficher(){
	pc1.afficheColor();
	pc2.afficheColor();
}
