/*
 * Triangle.h
 *
 *  Created on: 19 oct. 2016
 *      Author: user
 */

#include "Forme.h"
#include "PointColor.h"

#ifndef TRIANGLE_H_
#define TRIANGLE_H_

class Triangle : public Forme{
private:
	PointColor pc1;
	PointColor pc2;
	PointColor pc3;


public:
	Triangle(const PointColor& p1,const PointColor& p2, const PointColor& p3);
	virtual ~Triangle();

	void deplacer(float dx, float dy);
	void afficher();
};

#endif /* TRIANGLE_H_ */
