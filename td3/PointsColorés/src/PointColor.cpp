/*
 * PointColor.cpp
 *
 *  Created on: 19 oct. 2016
 *      Author: user
 */
#include <iostream>
using namespace std;
#include "PointColor.h"
//Constructeurs
PointColor::PointColor(Point &p, string c): Point(p.getX(),p.getY()),couleur(c){}
PointColor::PointColor(float x,float y,string c):Point(x,y),couleur(c){}// constructeur complet
PointColor::PointColor():Point(0,0),couleur("noir"){}//constructeur par defaut
PointColor::PointColor(const PointColor& p):Point(p.getX(),p.getY()),couleur(p.couleur){}

//Methode

void PointColor::afficheColor(){
	this->Afficher();
	cout << "La couleur est : " << couleur << endl;
}

//destructeur
PointColor::~PointColor() {
}
