/*
 * GroupeForme.h
 *
 *  Created on: 3 nov. 2016
 *      Author: user
 */

#ifndef GROUPEFORME_H_
#define GROUPEFORME_H_
#include <vector>
#include "Forme.h"

class GroupeForme : public Forme{
public:
	GroupeForme();
	virtual ~GroupeForme();

	void AjoutForme(Forme * f);
	void afficher();
	void deplacer(float dx, float dy);
private :
	vector<Forme*> vec;
};

#endif /* GROUPEFORME_H_ */
