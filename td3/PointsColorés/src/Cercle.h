/*
 * Cercle.h
 *
 *  Created on: 2 nov. 2016
 *      Author: Yoann
 */

#ifndef CERCLE_H_
#define CERCLE_H_

#include "Forme.h"
#include "PointColor.h"

class Cercle : public Forme {
private :
	PointColor pc;
	float rayon;

public:
	Cercle(const PointColor &p, const float &r);

	void deplacer(float dx, float dy);
	void afficher();

	virtual ~Cercle();
};

#endif /* CERCLE_H_ */
