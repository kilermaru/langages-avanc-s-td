/*
 * Cercle.cpp
 *
 *  Created on: 2 nov. 2016
 *      Author: Yoann
 */

#include "Cercle.h"

Cercle::Cercle(const PointColor &p, const float &r): pc(p), rayon(r) {}

void Cercle::deplacer(const float dx, const float dy){
	pc.deplacePoint(dx,dy);
}

void Cercle::afficher(){
	pc.afficheColor();
}

Cercle::~Cercle() {
	// TODO Auto-generated destructor stub
}
