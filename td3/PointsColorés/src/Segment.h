/*
 * Segment.h
 *
 *  Created on: 19 oct. 2016
 *      Author: user
 */

#include "Forme.h"

#ifndef SEGMENT_H_
#define SEGMENT_H_

class Segment : public Forme {
private :
	PointColor pc1;
	PointColor pc2;

public:
	Segment(const PointColor& p1,const PointColor& p2);
	Segment();
	virtual ~Segment();

	void deplacer(float dx, float dy);
	void afficher();

};

#endif /* SEGMENT_H_ */
