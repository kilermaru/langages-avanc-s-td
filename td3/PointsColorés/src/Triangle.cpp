/*
 * Triangle.cpp
 *
 *  Created on: 19 oct. 2016
 *      Author: user
 */

#include "Triangle.h"

Triangle::Triangle(const PointColor& p1,const PointColor& p2, const PointColor& p3): pc1(p1),pc2(p2),pc3(p3){}

Triangle::~Triangle() {
	// TODO Auto-generated destructor stub
}

void Triangle::deplacer(const float dx, const float dy){
	pc1.deplacePoint(dx,dy);
	pc2.deplacePoint(dx,dy);
	pc3.deplacePoint(dx,dy);
}

void Triangle::afficher(){
	pc1.afficheColor();
	pc2.afficheColor();
	pc3.afficheColor();
}
