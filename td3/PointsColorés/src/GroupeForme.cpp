/*
 * GroupeForme.cpp
 *
 *  Created on: 3 nov. 2016
 *      Author: user
 */

#include "GroupeForme.h"

GroupeForme::GroupeForme():vec(){}
GroupeForme::~GroupeForme() {
	// TODO Auto-generated destructor stub
}

void GroupeForme::AjoutForme(Forme * f){
	vec.push_back(f);
}
void GroupeForme::afficher(){
	for(vector<Forme*>::size_type i =0; i < vec.size(); i++){
		vec[i]->afficher();
	}
}
void GroupeForme::deplacer(float dx, float dy){
	for(vector<Forme*>::size_type i=0;  i< vec.size(); i++){
		vec[i]-> deplacer(dx, dy);
	}
}
