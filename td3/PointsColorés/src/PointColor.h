/*
 * PointColor.h
 *
 *  Created on: 19 oct. 2016
 *      Author: user
 */

#include <string>
#include "Point.h"
using namespace std;

#ifndef POINTCOLOR_H_
#define POINTCOLOR_H_

class PointColor : public Point
{
public:
	PointColor(Point& p, string c);
	PointColor(float x,float y,string c);
	PointColor(const PointColor& p);
	PointColor();

	virtual void afficheColor();

	virtual ~PointColor();

private:
	string couleur;
};

#endif /* POINTCOLOR_H_ */
