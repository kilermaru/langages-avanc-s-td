/*
 * Rectangle.cpp
 *
 *  Created on: 2 nov. 2016
 *      Author: Yoann
 */

#include "Rectangle.h"

Rectangle::Rectangle(const PointColor& p1,const PointColor& p2, const PointColor& p3, const PointColor& p4): pc1(p1),pc2(p2),pc3(p3),pc4(p4){}

void Rectangle::deplacer(float dx,float dy){
	pc1.deplacePoint(dx,dy);
	pc2.deplacePoint(dx,dy);
	pc3.deplacePoint(dx,dy);
	pc4.deplacePoint(dx,dy);
}

void Rectangle::afficher(){
	pc1.afficheColor();
	pc2.afficheColor();
	pc3.afficheColor();
	pc4.afficheColor();
}

Rectangle::~Rectangle() {
	// TODO Auto-generated destructor stub
}
