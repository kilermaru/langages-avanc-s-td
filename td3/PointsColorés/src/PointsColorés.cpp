//============================================================================
// Name        : PointsColorés.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <list>
using namespace std;

#include "PointColor.h"
#include "Segment.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "Cercle.h"
#include "GroupeForme.h"

//g++ Point.h Point.cpp PointColor.h PointColor.cpp Forme.h Forme.cpp Segment.h Segment.cpp Triangle.h Triangle.cpp Rectangle.h Rectangle.cpp Cercle.h Cercle.cpp PointsColor�s.cpp

int main() {
	cout << "Bonjour !!!" << endl; // prints !!!Hello World!!!
	//cout << "Creation d'un point couleur !" << endl;
	PointColor* p1 = new PointColor(1,2,"bleu");
	PointColor* p2 = new PointColor(10,20,"vert");
	PointColor* p3 = new PointColor(20,30,"rouge");
	PointColor* p4 = new PointColor(30,40,"jaune");
	/*cout << "Affichage de ce point : " << endl;
	p1->afficheColor();

	cout <<endl;

	cout << "contructeur de copie !!!" <<endl;
	PointColor* p2 = new PointColor(*p1);
	cout << "Affichage de ce nouveau point : " << endl;
	p2->afficheColor();

	cout <<endl;

	cout << "Autre point" <<endl;
	PointColor* p3 = new PointColor(10.5,95.2,"vert"); //quand constructeur();

	cout << endl;
	cout << endl;

	cout << "creation d'un segment" << endl;
	Segment* s1 = new Segment(*p1, *p3);
	cout << "Affichage" <<endl;
	s1->afficher();
	cout << "Deplacement dx=10 et dy=20" <<endl;
	s1->deplacer(10,20);
	s1->afficher();

	cout <<endl;

	cout << "constructeur d'un triangle p1=p3" <<endl;
	Triangle* t1 = new Triangle (*p1,*p3,*p1);
	t1->afficher();
	cout << "DEPLACEMENT dx et dy = 10" << endl;
	t1->deplacer(10,10);
	t1->afficher();

	cout << endl;

	cout << "constructeur Rectangle" << endl;
	Rectangle *r = new Rectangle(*p1,*p2,*p3,*p4);
	r->afficher();
	cout << "DEPLACEMENT DE 10"<< endl;
	r->deplacer(10,10);
	r->afficher();*/

	float r=50;
	Cercle *c=new Cercle(*p1,r);
	Segment *s = new Segment(*p1 , *p3);
	Rectangle *r1 = new Rectangle(*p1,*p2,*p3,*p4);
	GroupeForme *a = new GroupeForme();
	a->AjoutForme(c);
	a->AjoutForme(s);
	a->AjoutForme(r1);

	a->afficher();
	a->deplacer(4.23, 5.63);
	a->afficher();
	/*c->afficher();
	cout << "DEPLACEMENT" << endl;
	c->deplacer(10,10);
	c->afficher();*/

	return 0;
}
