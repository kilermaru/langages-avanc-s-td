/*
 * Forme.h
 *
 *  Created on: 19 oct. 2016
 *      Author: user
 */

#include "PointColor.h"

#ifndef FORME_H_
#define FORME_H_

class Forme {
public:
	virtual ~Forme();
	Forme();

	//fonction virtuelle pure
	virtual void deplacer(float dx, float dy) = 0;
	virtual void afficher() = 0;
};

#endif /* FORME_H_ */
