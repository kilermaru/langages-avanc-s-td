#include "CList.h"

using namespace std;

std::ostream& operator<<(std::ostream& flux, CList& cL)
{
    Liste tmp=cL.l;
    while(cL.l){
        flux << cL.l->val << "\t";
        cL.l=cL.l->suiv;
    }
    cL.l=tmp;
    return flux;
}


CList &CList::operator>(int& x)
{
    if(this->l==NULL)
        x=-1;
    else{
        x=this->l->val;
        this->l=this->l->suiv;
    }
    return *this;
}

CList::~CList()
{
    if(this->l){
    Liste tmp=this->l;
        while(tmp)
        {
            tmp=this->l->suiv;
            delete this->l;
        }
    }
}
