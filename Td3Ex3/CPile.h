#ifndef CPILE_H_INCLUDED
#define CPILE_H_INCLUDED
#include "CList.h"

class CPile: public CList{

public:
    CPile();
    CList &operator<(const int x);

    ~CPile();

};


#endif // CPILE_H_INCLUDED
