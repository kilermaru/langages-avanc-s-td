#include "CFile.h"

using namespace std;

CFile::CFile()
{
    this->l=NULL;
}

CList &CFile::operator<(const int x)
{
    Liste tmp=new Element();
    tmp->val=x;
    tmp->suiv=NULL;

    if(!(this->l))
        this->l=tmp;

    else{
        Liste l1=this->l;
        while(this->l->suiv)
            this->l=this->l->suiv;
        this->l->suiv=tmp;
        this->l=l1;
    }
   return *this;
}


CFile::~CFile()
{
}
