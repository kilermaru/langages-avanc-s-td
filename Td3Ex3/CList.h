#ifndef CLIST_H_INCLUDED
#define CLIST_H_INCLUDED

#include <iostream>
template<typename Type>
struct Element{
    Type val;
    Element* suiv;
    };
    typedef Element* Liste;
template<class Type>
class CList{

public:
    friend std::ostream& operator<<(std::ostream& flux, CList& l);
    virtual CList &operator<(const int x)=0;// on aurait pu d�clarer une methode ajouter virtual pure qui aurait permis de d�finir une partie de cet op�rateur � l'int�rieur meme de CList, on aurait alors juste eu � red�finir ajouter dans file et pile
    virtual CList &operator>(Type& x);
    virtual ~CList();
};
template<typename Type>
std::ostream& operator<<(std::ostream& flux, CList& cL)
{
    Liste tmp=cL.l;
    while(cL.l){
        flux << cL.l->val << "\t";
        cL.l=cL.l->suiv;
    }
    cL.l=tmp;
    return flux;
}

template<typename Type>
CList &CList::operator>(int& x)
{
    if(this->l==NULL)
        x=-1;
    else{
        x=this->l->val;
        this->l=this->l->suiv;
    }
    return *this;
}
template<typename Type>
CList::~CList()
{
    if(this->l){
    Liste tmp=this->l;
        while(tmp)
        {
            tmp=this->l->suiv;
            delete this->l;
        }
    }
}

#endif // CLIST_H_INCLUDED
