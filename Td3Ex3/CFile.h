#ifndef CFILE_H_INCLUDED
#define CFILE_H_INCLUDED
#include "CList.h"

class CFile: public CList{

public:
    CFile();
    CList &operator<(const int x);
    ~CFile();

};

#endif // CFILE_H_INCLUDED
