/*
 * vecteur.h
 *
 *  Created on: 6 oct. 2016
 *      Author: user
 */

#ifndef VECTEUR_H_
#define VECTEUR_H_

class vecteur {
public:
	vecteur(int nb);

	int operator[](int i);
	vecteur& operator+(const vecteur& a);
	vecteur& operator+=(const vecteur& a);
	vecteur& operator+=(const vecteur& a);

	virtual ~vecteur();

private:
	int size;
	int tab[];
};

#endif /* VECTEUR_H_ */
