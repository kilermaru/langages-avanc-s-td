//============================================================================
// Name        : exo1.cpp
// Author      : Corentin Briand
// Version     :
// Copyright   : © C.Briand 2016
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "CString.h"
using namespace std;
template <typename Type> Type GetMax(Type v1, Type v2){

	if (v1 > v2) return v1;
	else return v2;
}
int main() {
	int i=5, j=6, k;
	float l=10.0, m=5.0, n;
	CString s1("Toto"), s2("j"), o(GetMax(s1,s2));
	k = GetMax(i, j); //retourne le plus grand entre i et j
	n = GetMax(l, m); //retourne le plus grand entre l et m
	cout << k << endl;
	cout << n << endl;
	cout << o.getString <<endl;
	return 0 ;
}
