/*
 * CString.h
 *
 *  Created on: 29 sept. 2016
 *      Author: user
 */

#include <cstdio>
#include <cstring>
#ifndef CSTRING_H_
#define CSTRING_H_

class CString {
public:
	CString(const char* arg);
	CString(const char arg);
	CString(void);
	virtual ~CString();
	static int nbrChaines();
	CString plus(char c) const;
	bool plusGrandQue(const CString &s)const;
	bool infOuEgale(const CString &c) const;
	CString plusGrand(const CString &c) const;
	char* getString() const;

	CString operator=(CString const& c1);
	CString operator+(char const& c1);
	bool operator>(CString const& c1);
	bool operator<=(CString const& c1);

private:
	static int compteur;
	char* chaineDeCaractere;
};

#endif /* CSTRING_H_ */
