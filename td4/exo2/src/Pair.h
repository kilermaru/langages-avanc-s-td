/*
 * Pair.h
 *
 *  Created on: 10 nov. 2016
 *      Author: user
 */
#include <iostream>
using namespace std;
#ifndef PAIR_H_
#define PAIR_H

template <typename T>
class Pair {
private :
	T v1;
	T v2;
public:
	Pair(T v, T u);
	virtual ~Pair();
	T getMax();
};


template<typename T>
Pair<T>::Pair(T v, T u):v1(v),v2(u) {}

template<typename T>
Pair<T>::~Pair() {
	// TODO Auto-generated destructor stub
}

template<>
int Pair<int>::getMax(){
	if(v1 > v2){
		return v1;
	}
	else return v2;
}

template<>
float Pair<float>::getMax(){
	if(v1 > v2){
			return v1;
		}
		else return v2;
}

template<typename T>
T Pair<T>::getMax(){
	cout << "Erreur type non reconnu" << endl;
	return 0;
}
#endif /* PAIR_H_ */
