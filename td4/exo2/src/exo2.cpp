//============================================================================
// Name        : exo2.cpp
// Author      : Corentin Briand
// Version     :
// Copyright   : © C.Briand 2016
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
#include "Pair.h"

int main(){
	int i=5, j=6;
	float l=10.0, m=5.0;
	double z = 10.2232, u = 4510.23;
	Pair<double> myDouble(u, z);
	Pair<int> myInts (i,j); //créer une paire d’entiers
	Pair<float> myFloats (l,m); //créer une paire de flottants
	cout << myInts.getMax()<< endl; //affiche le plus grand des 2 entiers
	cout << myDouble.getMax() << endl;
	cout << myFloats.getMax()<<endl;//affiche le plus grand des 2 flottants
	return 0 ;
 }
