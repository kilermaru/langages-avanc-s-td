//============================================================================
// Name        : Exercice.cpp
// Author      : C.Briand
// Version     :
// Copyright   : C.Briand 2016
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	int tab[10] = {0,1,2,3,4,5,6,7,8,9};
	int *p = tab;
	int i;
	for (i=0; i <10 ; i++){
		cout << *p++ << "\n" << endl;
	}
	return 0;
}
