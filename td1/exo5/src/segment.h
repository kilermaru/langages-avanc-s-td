/*
 * segment.h
 *
 *  Created on: 29 sept. 2016
 *      Author: user
 */
#include "Point.h"
#include <iostream>
#ifndef SEGMENT_H_
#define SEGMENT_H_

class Segment {
	Point x;
	Point y;
public:
	Segment();
	Segment(Point x, Point y);
	Segment(Segment* a);
	virtual ~Segment();
	double longueur();
	friend std::ostream& operator << (ostream &os, const Segment &s);
private :
	bool estVertical();
	bool estHorizontal();
};

#endif /* segment_H_ */
