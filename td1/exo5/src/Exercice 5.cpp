//============================================================================
// Name        : Exercice.cpp
// Author      : C.Briand
// Version     :
// Copyright   : C.Briand 2016
// Description : Hello World in C++, Ansi-style
//============================================================================
#include<math.h>
#include <iostream>
using namespace std;
class Point {
	float x;
	float y;
	public :
		Point (){
			this-> x = 1.0;
			this-> y = 1.0;
		}
		Point (float x, float y){
			this->x = x;
			this->y = y;
		}
		Point (Point* a){
			this->x = a->x;
			this->y = a->y;
		}

		void afficher(){
			cout <<"x : " << this->x << " y : " << this->y << "\n" << endl;
		}
		~Point();
};
class segment{
	Point x;
	Point y;
public :
	~segment();
	segment(){
		this->x = new Point();
		this->y = new Point;
	}
	segment(Point x, Point y){
		this->x = x;
		this->y = y;
	}
	segment ( segment* a){
		this->x= a->x;
		this->y= a->y;
	}
	void longueur(){
		cout << "la longueur du segment est :" << sqrt((this->x.x - this->y.x)^2 + (this->x.y - this->y.y)^2);
	}
};

int main() {

	return 0;
}
