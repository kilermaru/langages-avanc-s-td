/*
 * segment.cpp
 *
 *  Created on: 29 sept. 2016
 *      Author: user
 */

#include "segment.h"

Segment::Segment() {
	this->x = new Point();
	this->y = new Point();
}
Segment::Segment(Point x, Point y){
		this->x = x;
		this->y = y;
}
Segment::Segment(Segment* a){
		this->x= a->x;
		this->y= a->y;
	}
Segment::~Segment() {
	delete(this->x);
	delete(this->y);
}
double Segment::longueur(){
	return sqrt((this->x.getX() - this->y.getX())^2 + (this->x.getY() - this->y.getY())^2);
}
bool Segment::estVertical(){
	if (this->x.getX() == this->y.getX()){
		return 1;
	}
	else return 0;
}
bool Segment::estHorizontal(){
	if (this->x.getY() == this->y.getY()){
		return 1;
	}
	else return 0;
}
std::ostream& operator << (ostream & os, const Segment &s){
	os<<"[" << s.x << "," << s.y << "]";
	return os;
}
}
