/*
 * Point.h
 *
 *  Created on: 29 sept. 2016
 *      Author: user
 */
#include <math.h>
using namespace std;


#ifndef POINT_H_
#define POINT_H_

class Point {
	float x;
	float y;
public:
	Point();
	Point(float x, float y);
	Point (Point* a);
	virtual ~Point();
	float getX();
	float getY();
	void afficher();
};

#endif /* POINT_H_ */
