//============================================================================
// Name        : Exo.cpp
// Author      : C.Briand
// Version     :
// Copyright   : C.Briand 2016
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	char * tab = new char*[3];
	tab[0] = "A";
	tab[1] = "B";
	tab[2] = "C";
	delete [] tab;
	//ou 
	string *t = new string[3];
	return 0;
}
