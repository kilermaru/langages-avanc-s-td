/*
 * Definition.cpp
 *
 *  Created on: 6 oct. 2016
 *      Author: user
 */

#include "Definition.h"

Definition::Definition(CString const& mot, CString const& def) {
	this->mot=mot;
	this->def=def;
}

char* Definition::getClef(){
	return this->mot.chaineDeCaractere;
}

char* Definition::getDef(){
	return this->def.chaineDeCaractere;
}

Definition::~Definition() {
	// TODO Auto-generated destructor stub
}

