/*
 * Dictionnaire.h
 *
 *  Created on: 15 oct. 2016
 *      Author: Yoann
 */
#include <list>
using namespace std;

#include "Definition.h"

#ifndef DICTIONNAIRE_H_
#define DICTIONNAIRE_H_

class Dictionnaire {
public:
	//Constructeurs
	Dictionnaire(); //constructeur d'un dictionnaire vide
	Dictionnaire(Definition d); //constructeur d'un dictionnaire avec une definition
	Dictionnaire(Dictionnaire d); //constructeur d'une copie d'un dictionnaire

	//m�thodes
	Dictionnaire ajoutDef(const Definition d)const;


	virtual ~Dictionnaire(); //destructeur d'un dictionnaire

private:
	list<Definition> dictionnaire;
};

#endif /* DICTIONNAIRE_H_ */
