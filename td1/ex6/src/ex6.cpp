//============================================================================
// Name        : ex6.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include"CString.h"
#include <iostream>
using namespace std;

int main()
{
	CString s1("toto");
	CString s2('q');
	CString s3;

	cout << "nbrChaines " << CString::nbrChaines() << endl ;
	s3 = s1.plus( 'w' ) ;
	cout << "s3=" << s3.getString() << endl ;
	if( s1.plusGrandQue(s2) ) // si s1 > s2 au sens alphabétique
		cout << "plus grand" << endl ;
	if( s1.infOuEgale(s2) ) // si s1 <= s2 au sens alphabétique
		cout << "plus petit" << endl ;
	s3 = s1.plusGrand( s2 ) ;// retourner s1 si s1>s2, s2 sinon
}
