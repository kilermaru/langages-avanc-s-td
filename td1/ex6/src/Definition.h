/*
 * Definition.h
 *
 *  Created on: 6 oct. 2016
 *      Author: user
 */

#ifndef DEFINITION_H_
#define DEFINITION_H_

class Definition {
public:
	Definition(CString const& mot, CString const& def);

	char* getClef();
	char* getDef();

	virtual ~Definition();

private:
	CString mot;
	CString def;
};

#endif /* DEFINITION_H_ */
