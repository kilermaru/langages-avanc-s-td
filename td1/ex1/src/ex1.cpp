//============================================================================
// Name        : ex1.cpp
// Author      : yobero
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include<string>

using namespace std;

int main() {
	int tab[10];
	int* p=tab;

	int i=0;
	while(i<10){
		//sorti standard en C++
		cout << *p << std::endl;
		p=p+1;
		i++;
	}
	cout << "fin 1)"<<std::endl;

	string* chaine[]=new string[3];
	chaine[0]="truc";
	chaine[1]="machin";
	chaine[2]="chose";

	delete[] chaine;

	return 0;
}
