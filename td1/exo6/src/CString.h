/*
 * CString.h
 *
 *  Created on: 29 sept. 2016
 *      Author: user
 */
#include <cstdio>
#include <iostream>
#include <cstring>
using namespace std;
#ifndef CSTRING_H_
#define CSTRING_H_

class CString {
	char* s;
	static int compteur;
public:
	CString(void);
	CString(const char s);
	CString(const char* S);
	static int nombreInstances();
	char* plus(char c);
	void afficher();
	bool plusGrandQue(const CString &s) const;
	virtual ~CString();
};

#endif /* CSTRING_H_ */
