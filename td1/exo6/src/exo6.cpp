//============================================================================
// Name        : exo6.cpp
// Author      : Corentin Briand
// Version     :
// Copyright   : C.Briand 2016
// Description : Hello World in C++, Ansi-style
//============================================================================


#include "CString.h"


int main() {
	CString s1("abcdefgh");
	CString s2("tot");
	CString s3 = new CString();
	cout << "nombre de chaines " << CString::nombreInstances() << endl; // prints !!!Hello World!!!
	s2.afficher();
	s3 = s1.plus('w');
	s3.afficher();
	return 0;
}
