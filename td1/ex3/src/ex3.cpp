//============================================================================
// Name        : ex3.cpp
// Author      : yobero
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

void tableauEcriture(int tab[]);
void tableauLecture(int tab[]);

int main() {

	int tab[10];
	tableauEcriture(tab);
	tableauLecture(tab);

	return 0;
}

void tableauEcriture(int tab[]){
	for(int i=0;i<10;i++){
		tab[i]=i;
	}
}

void tableauLecture(const int tab[]){
	for(int i=0;i<10;i++){
		cout << tab[i] << std::endl;
	}
}
