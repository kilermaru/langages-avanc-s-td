//============================================================================
// Name        : Exercice.cpp
// Author      : C.Briand
// Version     :
// Copyright   : C.Briand 2016
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
void echange(int &a, int &b){
	int tmp = a;
	a =b;
	b = tmp;
}
int main() {
	int a = 1, b = 2;
	cout << "a = " << a  << "\n "<< "b = " << b << "\n" << endl; // prints !!!Hello World!!!
	echange(a,b);
	cout << "a = " << a  << "\n "<< "b = " << b << "\n" << endl;
	return 0;
}
