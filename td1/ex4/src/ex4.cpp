//============================================================================
// Name        : ex4.cpp
// Author      : yobero
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Point.h"
using namespace std;

int main() {

	Point p1 = Point();
	Point p2 = Point(5,2);
	Point p3 = Point(p1);

	p1.Afficher();
	p2.Afficher();
	p3.Afficher();
	Point p4 = p2.cloner();
	p4.Afficher();

	return 0;
}
