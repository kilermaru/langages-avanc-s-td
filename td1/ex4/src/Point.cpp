/*
 * Point.cpp
 *
 *  Created on: 22 sept. 2016
 *      Author: user
 */

#include <iostream>
using namespace std;
#include "Point.h"

Point::Point() {
	// TODO Auto-generated constructor stub
	this->x=0;
	this->y=0;
}

Point::Point(float x,float y){
	this->x=x;
	this->y=y;
}

Point::Point(const Point &p){
	this->x = p.x;
	this->y = p.y;
}

void Point::Afficher(){
	cout << "les coordonnées sont x:" << this->x << " y:" << this->y << endl;
}

Point Point::cloner(){
	Point nouv;
	nouv.x=this->x;
	nouv.y=this->y;
	return nouv;
}

Point::~Point() {
	// TODO Auto-generated destructor stub

}

