/*
 * Point.h
 *
 *  Created on: 22 sept. 2016
 *      Author: user
 */
#pragma once

#ifndef POINT_H_
#define POINT_H_

#include <iostream>
using namespace std;

class Point {
public:
	Point();
	virtual ~Point();

	float x;
	float y;

	Point(float x,float y);
	Point (const Point &p);

	void Afficher();
	Point cloner();
};

#endif /* POINT_H_ */
