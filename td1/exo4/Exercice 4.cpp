//============================================================================
// Name        : Exercice.cpp
// Author      : C.Briand
// Version     :
// Copyright   : C.Briand 2016
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
class Point {
	float x;
	float y;
	public :
		Point (){
			this-> x = 1.0;
			this-> y = 1.0;
		}
		Point (float x, float y){
			this->x = x;
			this->y = y;
		}
		Point (Point* a){
			this->x = a->x;
			this->y = a->y;
		}

		void afficher(){
			cout <<"x : " << this->x << " y : " << this->y << "\n" << endl;
		}
		Point cloner(const Point &a){
			this-> x = a.x;
			this-> y = a.y;
		}
		~Point(){
			cout << "destruction de ce fdp \n"; // destructeur
		}
};
int main() {
	Point* p = new Point(4,5.0312);
	Point * c = new Point();
	p->afficher();
	p->cloner(c);
	p->afficher();
	delete(c);
	delete(p);
	return 0;
}
