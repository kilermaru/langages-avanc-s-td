/*
 * Segment.cpp
 *
 *  Created on: 29 sept. 2016
 *      Author: user
 */

#include "Segment.h"

Segment::Segment():p1(0,0), p2(0,0){
	cout << "Appel du constructeur 1" << endl;
}

Segment::Segment(const Point p1,const Point p2) {
	// TODO Auto-generated constructor stub
	cout << "Appel du constructeur 2" << endl;
	this->p1=p1;
	this->p2=p2;
}

Segment::~Segment() {
	// TODO Auto-generated destructor stub
	cout<<"appel du destructeur Segment()"<< endl;
}

double Segment::longueur(){
	cout << "voir théoreme de pythagore" <<endl;
}

bool Segment::estVertical(){
	if(p1.x == p2.x)
		return true;
	return false;
}

bool Segment::estHorizontal(){
	if(p1.y == p2.y)
		return true;
	return false;
}

bool Segment::estSurDiagonal(){
	cout << "voir sur internet :D" << endl;
}



