/*
 * Segment.h
 *
 *  Created on: 29 sept. 2016
 *      Author: user
 */

#include "Point.h"
#ifndef SEGMENT_H_
#define SEGMENT_H_

class Segment {
public:
	Segment();
	Segment(const Point p1,const Point p2);
	virtual ~Segment();

	double longueur();
	bool estVertical();
	bool estHorizontal();
	bool estSurDiagonal();

private:
	Point p1;
	Point p2;
};

#endif /* SEGMENT_H_ */
