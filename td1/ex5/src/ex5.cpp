//============================================================================
// Name        : ex5.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include "Segment.h"
#include <iostream>
using namespace std;

int main() {
	Point* p1 = new Point(5,10);
	Point* p2 = new Point(10,45);
	cout << p1->x << p1->y << endl;
	cout << p2->x << p2->y << endl;

	Segment* s = new Segment(*p1,*p2);
	cout << s->estHorizontal() << endl;
	cout << s->estSurDiagonal() << endl;
	cout << s->estVertical() << endl;
	delete s;

	return 0;
}
