/*
 * CString.cpp
 *
 *  Created on: 29 sept. 2016
 *      Author: user
 */

#include "CString.h"
int CString::compteur = 0;
//  ***************************** contructeurs
CString::CString() {
	this->s = new char(1);
	this->s[0]= '\0';
	this->compteur ++;
}
CString::CString(const char* pt){
	this->s = new char (strlen(pt) +1);
	strcpy(this->s, pt);
	this->compteur++;
}
CString::CString(const char pt){
	this->s= new char (2);
	this->s[0] = pt;
	this->s[1] = '\0';
	this->compteur++;
}
CString::CString (const CString& a){
	s = new char(strlen(a.s)+1);
	strcpy(s, a.s);
	compteur++;
}
// *************************** destructeur
CString::~CString() {
	if(s){
		delete[] s;
	}
	compteur --;
	// TODO Auto-generated destructor stub
}
// *************************** Nombre d'Chaines
int CString::nombreChaines(){
	return compteur;
}
// *************************** Recuperation string
char* CString::getString(){
	return this->s;
}
// *************************** Addition
char* CString::plus(char c){
	size_t ldata = strlen(this->s);
	char* cp = new char (ldata + 2);
	strcpy(cp, this->s);
	cp [ldata] = c;
	cp [ldata+1] = '\0';
	return cp;
}
// ************************** Affichage
void CString::afficher(){
	cout << this->s << "\n" << endl;
}
// ************************** Comparaisons de taille
// 1)
// Supérieur
bool CString::plusGrandQue(const CString &s) const{
	if (strcmp(this->s, s.s) > 0)
			return true;

	return false;
}
bool CString::operator> (CString const& b){
	return !plusGrandQue(b);
}
//2)
//inf ou egal
bool CString::infOuEgal(const CString s) const{
	return !plusGrandQue(s);
}
bool CString::operator<=(CString const &b){
	return infOuEgal(b);
}
//3)
// Egal

CString& CString::operator= (const CString &b){
	delete[] s;
	s = b.s;
	return* this;
}
char * CString::plusGrand(const CString s) const{
	if (plusGrandQue(s))
		return this->s;
	return s.s;
}
