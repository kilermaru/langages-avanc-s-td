/*
 * Dictionnaire.h
 *
 *  Created on: 13 oct. 2016
 *      Author: user
 */

#ifndef DICTIONNAIRE_H_
#define DICTIONNAIRE_H_
#include"Definition.h"
#include <list>
class Dictionnaire {
private :
	std::list<Definition> d;
public:
	Dictionnaire(list<Definition> a);
	Dictionnaire(Definition x);
	virtual ~Dictionnaire();
};

#endif /* DICTIONNAIRE_H_ */
