/*
 * Definition.h
 *
 *  Created on: 6 oct. 2016
 *      Author: Corentin Briand
 */

#ifndef DEFINITION_H_
#define DEFINITION_H_
#include "CString.h"
class Definition {
private :
	CString mot;
	CString def;

public:
	Definition();
	Definition(const CString mot,const CString def);
	Definition(CString mot);
	virtual ~Definition();
};

#endif /* DEFINITION_H_ */
