/*
 * CString.h
 *
 *  Created on: 29 sept. 2016
 *      Author: user
 */
#include <cstdio>
#include <iostream>
#include <cstring>
#include <string.h>
using namespace std;
#ifndef CSTRING_H_
#define CSTRING_H_

class CString {
	char* s;
	static int compteur;
public:
	CString(void);
	CString(const char s);
	CString(const char* S);
	CString (const CString& a);
	static int nombreChaines();
	char* getString();
	char* plus(char c);
	void afficher();
	bool plusGrandQue(const CString &s) const;
	bool operator> (CString const& b);
	bool infOuEgal(const CString s) const;
	bool operator<= (const CString &b);
	char* plusGrand(const CString s) const;
	CString& operator= (const CString &b);
	virtual ~CString();
};

#endif /* CSTRING_H_ */
